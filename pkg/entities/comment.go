package entities

type CommentEntity struct {
	ID   int    `gorm:"primaryKey" json:"id"`
	PID  int    `gorm:"column:pid" json:"pid"`
	UID  int    `gorm:"column:uid" json:"uid"`
	Text string `gorm:"column:text" json:"text"`
}

func (CommentEntity) TableName() string { // Solo se utiliza esta función para definir el nombre de la tabla en la base de datos
	return "comments"
}
