package db

import (
	"fmt"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/internal/config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB // Se hace aquí para que sea global y poder exportarla

func DBConection(config *config.DBConfigs) *gorm.DB {
	fmt.Println("DB connecting...")
	var err error
	DB, err = gorm.Open(mysql.Open(config.GetConnectionString()), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	fmt.Println("DB connected")
	return DB // Retornando la conexión
}
