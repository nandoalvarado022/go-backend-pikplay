package routes

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

func GetTasks(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get all tasks")
	w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode([]string{"Task 1", "Task 2", "Task 3"})

	// Probando contextos
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // Se hace el defer cancel para que se ejecute cancel cuando se termine la función
	go work(ctx, "f1")
	go work(ctx, "f2")
	go work(ctx, "f3")
	go work(ctx, "f4")
	go work(ctx, "f5")

	time.AfterFunc(time.Second, func() {
		cancel() // Cancelamos el contexto después de 1 segundo
	})
	time.Sleep(3 * time.Second) // Simulamos que la tarea dura 3 segundos
}

func work(ctx context.Context, name string) {
	workdone := make(chan struct{}) // Creamos un canal de tipo struct. Nos sirve para comunicar que hemos terminado con la tarea
	go func() {
		// Leer un archivo por ejemplo o nuestro código
		time.Sleep(2 * time.Second) // Simulamos que la tarea dura 2 segundos
		workdone <- struct{}{}
	}()

	select {
	case <-workdone: // De workdone vamos a esperar a que se termine
		fmt.Println("Terminó ", name)
		return
	case <-ctx.Done(): // De ctx vamos a esperar a que se termine
		fmt.Printf("%s %v\n", name, ctx.Err()) // Imprimimos el error que nos devuelve el contexto
		return
	}
	// go fmt.Println("Working", name)
}
