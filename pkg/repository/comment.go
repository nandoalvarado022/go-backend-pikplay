// Los repository establecen connexion con la BD
package repository

import (
	"context"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/internal/db"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/pkg/entities"
)

func (r *Repository) GetComments(ctx context.Context, pid int) ([]entities.CommentEntity, error) {
	var comments []entities.CommentEntity
	r.db.Find(&comments)
	return comments, nil
}

func CreateComment(commentData entities.CommentEntity) (*entities.CommentEntity, error) {
	resultCreate := db.DB.Create(&commentData)
	err := resultCreate.Error
	if err != nil {
		// Error creando el comentario
		return nil, err
	}
	return &commentData, nil
}

/*
func (r *CommentRepository) Get(comment models.Comment) (*models.Comment, error) {
	commentType := &models.Comment{}
	resp := r.conn.Find(commentType, "pid = ? and status = 1", comment.PID)
	if resp.Error != nil {
		return nil, resp.Error
	} else {
		if resp.RowsAffected == 0 {
			return nil, errors.New("No se encontraron comentarios")
		}
	}
	return commentType, nil

	// r.conn.Find(commentRes)
	/*return http.Response{
		StatusCode: http.StatusCreated,
	}, nil
}*/
