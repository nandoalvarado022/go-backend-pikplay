// https://www.youtube.com/watch?v=B6gQ1B0cn4s&t=1429s

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/rest"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/internal/config"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/internal/db"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/pkg/entities"
)

type task struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Content string `json:"content"`
}

type allTasks []task

var tasks = allTasks{
	{
		ID:      1,
		Name:    "Task One",
		Content: "Some Content",
	},
}

func indexRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to my API")
}

func createTask(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Create task")
	var newTask task
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Insert a valid task")
	}
	newTask.ID = len(tasks) + 1
	json.Unmarshal(reqBody, &newTask) // Asignando el reqBody a la variable newTask
	tasks = append(tasks, newTask)
	w.WriteHeader(http.StatusCreated)                  // Para decirle que se ha creado un nuevo recurso
	w.Header().Set("Content-Type", "application/json") // Para decirle que el contenido que va a devolver es de tipo json. Sirve para que por ejemplo en postman sepa que el contenido que va a devolver es de tipo json y lo muestre pretty.
	json.NewEncoder(w).Encode(newTask)
}

func getTransactions() {

}

func getPublication(pid int) {
	var publication entities.Publication
	res := db.DB.Where("pid = ?", pid).Limit(1).Find(publication)
	if res.RowsAffected == 0 {
		fmt.Println("No se encontró la publicación")
		return
	}
	fmt.Println("La publicación es:")
	fmt.Println(publication)
	fmt.Println("Fin")
}

func getPublications() {
	var publications []entities.Publication
	if db.DB.Find(&publications).Error != nil {
		fmt.Println("Error")
		return
	}
	for _, publication := range publications {
		fmt.Println(publication)
	}
}

func getMetrics(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) // Obteniendo los parámetros de la url
	pid, _ := strconv.Atoi(vars["pid"])
	go getPublication(pid)
	// go getPublications()
	go getTransactions()
	fmt.Println("Fin de la función")
}

func getTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) // Obteniendo los parametros de la url
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		fmt.Fprintf(w, "Invalid ID")
		return
	}

	w.Header().Set("Content-Type", "application/json")
	for _, task := range tasks {
		if task.ID == id {
			json.NewEncoder(w).Encode(task)
		}
	}
}

func deleteTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		fmt.Fprintf(w, "Invalid ID")
		return
	}

	for i, task := range tasks {
		if task.ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...) // Eliminando el elemento del slice. Con el :i se añade los elementos que esten antes del indice i y con el i+1 se añaden los elementos que esten despues del indice i.
			fmt.Fprintf(w, "The task with ID %v has been removed successfully", id)
		}
	}
}

func main() {
	tracer.Start(
		tracer.WithEnv("prod"),
		tracer.WithService("Golang-API"),
		tracer.WithServiceVersion("1.0.0"),
	)

	// When the tracer is stopped, it will flush everything it has to the Datadog Agent before quitting.
	// Make sure this line stays in your main function.
	defer tracer.Stop()

	// Saying HI
	ctx := context.Background()
	fmt.Println("Hello, playground")

	// DB connection
	myConfig, err := config.LoadBDConfigs()
	if err != nil {
		log.Fatalln("Error cargando la config de la base de datos", err.Error())
	}

	/*
		Probando BD sin rutas
		repo := repository.NewRepository(db.DBConection(myConfig))
		comments, err := repo.GetComments(ctx, 1)
		if err != nil {
			panic(err)
		}
		fmt.Println(comments)
	*/
	// Para migraciones
	// db.DB.AutoMigrate(models.Activities{})   // Importando el modelo Tickets. Va a crear la tabla tickets en la base de datos
	// db.DB.AutoMigrate(models.Participants{})

	router := mux.NewRouter().StrictSlash(true)
	router.Use(customMiddleware)
	rest.SettingRoutes(ctx, router, myConfig) // Llamando a las rutas
	router.HandleFunc("/", indexRoute)
	http.ListenAndServe(":3001", router)
}

func customMiddleware(next http.Handler) http.Handler {
	// Seteando el header Content-Type a application/json
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
