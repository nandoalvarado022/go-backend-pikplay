package entities

import "gorm.io/gorm"

type Activities struct {
	gorm.Model // Va a leer nuestro struct y va a crear las columnas de la base de datos automáticamente

	ID           int            `gorm:"primaryKey"`
	Name         string         `gorm:"not null;unique_index"`
	Paid         bool           `gorm:"default:false"`
	Participants []Participants `gorm:"foreignKey:ActivityID"`
}
