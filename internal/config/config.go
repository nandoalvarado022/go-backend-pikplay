package config

import (
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"os"
)

type DBConfigs struct {
	Host     string
	User     string
	Password string
}

func LoadBDConfigs() (*DBConfigs, error) {
	path := "cmd/db_config.json"
	configs := &DBConfigs{}
	file, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("No se puede abrir el archivo de configuraciones de BD. %s", err.Error())
	}
	if err := json.Unmarshal(file, configs); err != nil {
		return nil, fmt.Errorf("archivo de configuraciones de BD incorrecto!, Error: %s", err.Error())
	}
	return configs, nil
}

func (d *DBConfigs) GetConnectionString() string {
	err := godotenv.Load()
	if err != nil {
		panic("Error loading .env file")
	}
	cadena := fmt.Sprintf("%s:%s@tcp(%s)/pikplay?charset=utf8mb4&parseTime=True&loc=Local", d.User, d.Password, d.Host)
	return cadena
}
