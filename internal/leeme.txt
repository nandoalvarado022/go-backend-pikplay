// El folder internal limita la visibilidadal propio paquete y subpaquetes
// Organiza el codsigo interno sin exponer detalles de implementacion
// Evita dependencias no deseadasy mantiene la separacion entra las interfaces publicas y privadas
// Son cosas que no se van a reutilizar en otro proyecto
package internal
