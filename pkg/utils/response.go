package utils

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

type Response struct {
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func OKResponse(c echo.Context, payload interface{}) error {
	return c.JSON(http.StatusOK, payload)
}
