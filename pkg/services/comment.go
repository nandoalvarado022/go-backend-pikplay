package services

import (
	"context"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/internal/config"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/internal/db"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/pkg/entities"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/pkg/repository"
)

// Siempre vamos a tener como dependencia el repositorio
type CommentService struct {
	repository *repository.Repository
}

func NewCommentService(myConfig *config.DBConfigs) *CommentService { // Esto es el constructor y retornamos un puntero
	return &CommentService{
		repository: repository.NewRepository(db.DBConection(myConfig)), // Aquí se inyecta la dependencia
		// Esta seria otra forma de hacerlo
	}
}

func (s *CommentService) Get(ctx context.Context, pid int) ([]entities.CommentEntity, error) {
	return s.repository.GetComments(ctx, pid) // Se propaga lo que me devuelve
}
