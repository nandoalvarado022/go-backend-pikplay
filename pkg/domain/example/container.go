package example

import (
	"context"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/pkg/entities"
)

type Container struct {
	Repository Repository
}

type Repository interface {
	GetComments(ctx context.Context, pid int) ([]entities.CommentEntity, error)
}

func NewContainer(repository Repository) *Container {
	return &Container{
		repository,
	}
}
