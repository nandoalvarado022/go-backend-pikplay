package rest

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/internal/config"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/rest/comment"
)

func SettingRoutes(ctx context.Context, router *mux.Router, myConfig *config.DBConfigs) {
	// router.HandleFunc("/tasks", routes.GetTasks).Methods("GET")
	// router.HandleFunc("/task/{id}", getTask).Methods("GET")
	// router.HandleFunc("/tasks", createTask).Methods("POST")
	// router.HandleFunc("/task/{id}", deleteTask).Methods("DELETE")
	// Comments
	commentHandler := comment.NewCommentHandler(myConfig)
	router.HandleFunc("/comments", commentHandler.Get).Methods("GET")
}
