package example

import (
	"context"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/pkg/entities"
)

type Service interface {
	GetComments(ctx context.Context, pid int) ([]entities.CommentEntity, error)
}

type service struct {
	*Container
}
