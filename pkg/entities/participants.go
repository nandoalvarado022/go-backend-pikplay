package entities

import "gorm.io/gorm"

type Participants struct {
	gorm.Model

	ActivityID int `gorm:"not null"`
	UserID     int `gorm:"not null"`
}
