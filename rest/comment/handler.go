package comment

import (
	"encoding/json"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/internal/config"
	"gitlab.com/nandoalvarado022/go-backend-pikplay/pkg/services"
	"net/http"

	"gitlab.com/nandoalvarado022/go-backend-pikplay/pkg/entities"
)

type CommentHandler struct {
	service *services.CommentService
}

func NewCommentHandler(myConfig *config.DBConfigs) *CommentHandler {
	return &CommentHandler{
		service: services.NewCommentService(myConfig),
	}
}

func (h *CommentHandler) Get(w http.ResponseWriter, r *http.Request) {
	var comments []entities.CommentEntity
	comments, _ = h.service.Get(r.Context(), 1)
	json.NewEncoder(w).Encode(&comments)
}

/*func CreateCommentHandler(w http.ResponseWriter, r *http.Request) {
	var commentData entities.CommentEntity
	err := json.NewDecoder(r.Body).Decode(&commentData)
	if err != nil {
		w.Write([]byte("No coincide la entidad"))
		return
	}
	resultCreate, err := repository.CreateComment(commentData)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error creando el comentario"))
		return
	}
	err = json.NewEncoder(w).Encode(&resultCreate) // Devolviendo el comentario creado
	if err != nil {
		w.Write([]byte("Error devolviendo el comentario creado"))
		return
	}
}
*/
