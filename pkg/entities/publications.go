package entities

type Publication struct {
	PID   int    `gorm:"primaryKey"`
	Title string `gorm:"not null"`
	// Comments []Comment `gorm:"not null"`
}
